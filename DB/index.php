<?php
session_start();
/**
 * Connection DB
 */
$bdd = new PDO('mysql:host=127.0.0.1;dbname=server20-21;charset=utf8', 'root', 'root');
// A faire uniquement en developpement, sinon pas d'erreurs sql affichées
$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

/**
 * Comprendre ce que l'utilisateur veut faire, puis s'occupper des données, ensuite de l'affichage (via $content)
 */
$action = 'list';
if (isset($_GET['action'])) {
    $action = $_GET['action'];
}

$content = '';
if ($action == 'list') {
    if(isset($_GET['console'])) {
        // Mauvais, pas sécurisé, garde à l'injection SQL
//    $request = $bdd->query('SELECT * FROM `jeux_video` WHERE `id`='.$_GET['id']);
        // Bien, les valeurs dynamqiues de la requêtes sont présentées sous forme de paramètre (:nomDuParamètre)
//    $request = $bdd->prepare('SELECT * FROM `jeux_video` ORDER BY `nom` ASC');
        $request = $bdd->prepare('SELECT * FROM `jeux_video` WHERE `console` = :console');
        // On donne des valeurs aux paramètres avec bindParam
        $request->bindParam('console', $_GET['console']);
    } else {
        // ici, la methode prepare de PDO retourne un object de type PDOStatement
        $request = $bdd->prepare('SELECT * FROM `jeux_video`');
    }
    // Ensuite on execute la requête
    // ici, la methode prepare de PDO retourne un object de type PDOStatement
    $request->execute();
    // La methode fetchAll n'existe que sur les objects de type PDOStatement, si il y a une erreur dans la ligne précédente,
    // je n'aurai pas le bon object pour faire un fetchAll()
    $lines = $request->fetchAll();
    $content = getTable($lines);

}
else if ($action == 'create') {
    $consoles = fetchConsoles($bdd);
    if (isFormSubmit()) {
        if (isFormValid($consoles)) {
            $filePath = uploadFile();
            $request = $bdd->prepare('INSERT INTO `jeux_video` (`nom`, `console`, `prix`, `nbre_joueurs_max`, `possesseur` , `commentaires`, `image`) VALUES (:name, :console, :price, :maxPlayers, :owner, :comment, :image)');
            $params = [
                'name' => $_POST['name'],
                'console' => $_POST['console'],
                'price' => $_POST['price'],
                'maxPlayers' => $_POST['maxPlayers'],
                'owner' => $_POST['owner'],
                'comment' => $_POST['comment'],
                'image' => $filePath
            ];
            if ($request->execute($params)) {
                writeServiceMessage('Jeu '.$_POST['name'].' créé avec succès \\o/');
                header('Location: /DB'); // redirection
                die();
            }
        }
    }
    $content = getForm($consoles, null);
}
else if ($action == 'delete') {
    if (!isset($_GET['id'])) {
        http_response_code(400);
        $content = 'Mauvaise requête, impossible de supprimer sans avoir un id. <a href="/DB">Retour à la liste</a>';
    }else {
        $request = $bdd->prepare('DELETE FROM `jeux_video` WHERE `id`=:id');
        $params = ['id' => $_GET['id']];
        if ($request->execute($params)) {
            writeServiceMessage('Jeu '.$_POST['id'].' supprimé avec succès :(');
            header('Location: /DB'); // redirection
            die();
        }
    }
}
else if ($action == 'update') {
    if (!isset($_GET['id'])) {
        http_response_code(400);
        $content = 'Mauvaise requête, impossible de mettre à jour sans avoir un id. <a href="/DB">Retour à la liste</a>';
    } else {
        $consoles = fetchConsoles($bdd);
        if (isFormSubmit()) {
            if (isFormValid($consoles)) {
                $filePath = uploadFile();
                $imageUpdate = '';
                if ($filePath !== null) {
                    $imageUpdate = ', `image`=:image';
                }
                $request = $bdd->prepare('UPDATE `jeux_video` SET `nom`=:name, `console`=:console, `prix`=:price, `nbre_joueurs_max`=:maxPlayers, `possesseur`=:owner , `commentaires`=:comment '.$imageUpdate.' WHERE `id`=:id');
                $params = [
                    'name' => $_POST['name'],
                    'console' => $_POST['console'],
                    'price' => $_POST['price'],
                    'maxPlayers' => $_POST['maxPlayers'],
                    'owner' => $_POST['owner'],
                    'comment' => $_POST['comment'],
                    'id' => $_GET['id']];
                if($filePath !== null) {
                    $params['image'] = $filePath;
                }
                if ($request->execute($params)) {
                    writeServiceMessage('Jeu '.$_POST['name'].' mis à jour avec succès \\o/');
                    header('Location: /DB'); // redirection
                    die();
                }
            }
        }
        $request = $bdd->prepare('SELECT * from `jeux_video` WHERE `id`=:id');
        $request->execute(['id' => $_GET['id']]);
        $lines = $request->fetchAll();
        if (!count($lines)){
            http_response_code(404);
            $content = 'Données introuvables <a href="/DB">Retour à la liste</a>';
        }else {
            $content = getForm($consoles, $lines[0]);
        }
    }

}


echo '<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    </head>
    <body>';

echo getNav();
echo '<div class="container">';
$message = getServiceMessage();
if ($message) {
   echo '<div class="alert alert-primary" role="alert">
        '.$message.'
</div>';
}
echo $content;
echo '</div>';

echo '</body></html>';


function getForm($consolesList, $jeuVideo){
    $form = '
        <h1>Ajouter un jeu vidéo</h1>
        <form method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="name">Nom</label>
                <input class="form-control" type="text" name="name" name="name" value="'.($jeuVideo ? $jeuVideo['nom'] : '').'" required>
            </div>
            <div class="form-group">
                <label for="console">Console</label>
                <select class="form-control" name="console" id="console">';
                foreach ($consolesList as $console) {
                    $form .= '
                    <option value="'.$console.'" '.($jeuVideo && $jeuVideo['console'] === $console ? 'selected' : '').' >'.$console.'</option>';
                }
                $form .= '
                </select>
            </div>
            <div class="form-group">
                <label for="price">Prix</label>
                <input class="form-control" type="number" step="0.01" name="price" id="price"  value="'.($jeuVideo ? $jeuVideo['prix'] : '').'"required>
            </div>
            <div class="form-group">
                <label for="maxPlayers">Nombre de joueurs maximum</label>
                <input  class="form-control" type="number" name="maxPlayers" id="maxPlayers"  value="'.($jeuVideo ? $jeuVideo['nbre_joueurs_max'] : '').'" required>
            </div>
            <div class="form-group">
                <label for="owner">Possesseur</label>
                <input class="form-control" type="text" name="owner" id="owner"  value="'.($jeuVideo ? $jeuVideo['possesseur'] : '').'"required>
            </div>
            <div class="form-group">
                <label for="comment">Commentaires</label>
                <textarea class="form-control" name="comment" id="comment">'.($jeuVideo ? $jeuVideo['commentaires'] : '').'</textarea>
            </div>
            <div class="form-group">
                <label for="image">Image</label>
                <input type="file" name="image" id="image" '.($jeuVideo ? 'disabled="true"': '').' />';
            if ($jeuVideo) {
                $form.= '<img src="/DB/'.$jeuVideo['image'].'" />';
            }
    $form.= '
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="submit">Envoyer</button>
            </div>
        </form>
        <script src="/DB/images.js"></script>
    ';

    return $form;
}

function isFormSubmit() : bool {
    return isset($_POST['name']); // Si on a au moins un POST['name'] c'est que normalement le formulaire a été posté.
}

function isFormValid($consoles) : bool {
    $valid =
        !empty($_POST['name'])
        && in_array($_POST['console'], $consoles)
        && !empty($_POST['price']) && is_numeric($_POST['price'])
        && !empty($_POST['maxPlayers']) && is_numeric($_POST['maxPlayers'])
        && !empty($_POST['owner']) ;

        // Validation du fichier
        if ($valid && isset($_FILES['image']) AND $_FILES['image']['error'] == 0) {
            // Testons si le fichier n'est pas trop gros
            if ($_FILES['image']['size'] > 1000000) {
                writeServiceMessage("File to large");
                $valid = false;
            }
            // Testons si l'extension est autorisée
            $infosfichier = pathinfo($_FILES['image']['name']);
            $extension_upload = $infosfichier['extension'];
            $extensions_autorisees = array('jpg', 'jpeg', 'gif', 'png');
            if (!in_array($extension_upload, $extensions_autorisees))
            {
                writeServiceMessage("Only ".implode(", ", $extensions_autorisees). " files are allowed");
                $valid = false;
            }
        }
        return $valid;
}

function getNav() {
    $nav = '
    <nav>
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="?action=list">Voir la liste</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="?action=create">Ajouter un jeu</a>
          </li>
      </ul>
    </nav>
    ';

    return $nav;
}

function getTable($lines) {
    $table = '<h1>Liste des jeux vidéos</h1>';
    $table .= '<table class="table">';
    $table .= '<thead><tr><th>id</th><th><a href="?sort=nom">nom</a></th><th>console</th><th>prix</th><th>nomde de joueurs maximum</th><th>propriétaire</th><th>commentaires</th><th>image</th><th>Actions</th></tr></thead>';
    $table .= '<tbody>';
    foreach ($lines as $line) {
        $table .= '<tr>';
        $table .= '<td>'.$line['ID'].'</td>';
        $table .= '<td>'.$line['nom'].'</td>';
        $table .= '<td><a href="?console='.$line['console'].'">'.$line['console'].'</a></td>';
        $table .= '<td>'.$line['prix'].'</td>';
        $table .= '<td>'.$line['nbre_joueurs_max'].'</td>';
        $table .= '<td>'.$line['possesseur'].'</td>';
        $table .= '<td>'.$line['commentaires'].'</td>';
        $table .= '<td>'.($line['image'] !== null ? '<img src="DB/'.$line['image'].'" />' : '').'</td>';
        $table .= '<td>
                        <a class="btn btn-danger" href="?action=delete&id='.$line['ID'].'"><i class="fa fa-times"></i></a>
                        <a class="btn btn-primary" href="?action=update&id='.$line['ID'].'"><i class="fa fa-edit"></i></a>
                    </td>';
        $table .= '</tr>';
    }
    $table .= '</tbody>';
    $table .= '</table>';

    return $table;
}

function fetchConsoles(PDO $bdd): array {
    $req = $bdd->prepare('SELECT DISTINCT `console` from `jeux_video`');
    $req->execute();
    $results = $req->fetchAll();
    return array_map(function($consoleWrapper){ return $consoleWrapper['console'];}, $results);
}

function writeServiceMessage($message) {
    $_SESSION['serviceMessage'] = $message;
}

function getServiceMessage() {
    $message = null;
    if (isset($_SESSION['serviceMessage'])) {
        $message = $_SESSION['serviceMessage'];
        unset($_SESSION['serviceMessage']);
    }

    return $message;
}

function uploadFile() {
    if (isset($_FILES['image'])) {
        $filePath = './uploads/'.uniqid().basename($_FILES['image']['name']);
        if(move_uploaded_file($_FILES['image']['tmp_name'], $filePath)) {
            return $filePath;
        }
    }
    return null;
}

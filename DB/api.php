<?php
/**
 * Connection DB
 */
$bdd = new PDO('mysql:host=127.0.0.1;dbname=server20-21;charset=utf8', 'root', 'root');
// A faire uniquement en developpement, sinon pas d'erreurs sql affichées
$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//header('Content-Type: application/json');
//header('Referrer-Policy: origin-when-cross-origin');
header('Access-Control-Allow-Headers: *');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
//date_format('Y-m-d H:i:s',new DateTime());
$resource = null;
if (isset($_GET['resource'])) {
    $resource = $_GET['resource'];
}
// On aurait pu écrire
// $resource = isset($_GET['resource']) ? $_GET['resource'] : null;

if ($resource == 'jeux') {
    $request = $bdd->prepare('SELECT * FROM `jeux_video`');
    $request->execute();
    $data = $request->fetchAll();
    echo json_encode($data);
} else if ($resource == 'contact' && $_SERVER['REQUEST_METHOD'] === 'POST' ) {
    $inputJSON = file_get_contents('php://input'); // Moyen de récupérer le contenu du corps d'une requete.
    $contactForm = json_decode($inputJSON, true); // On decode ls json pour récupérer un tableau associatif. A partir d'ici on fait comme d'habitude quand on a un $_POST
    var_dump($contactForm);
    $imageLink = './uploads/'.uniqid().substr($contactForm['imageName'], strrpos($contactForm['imageName'], '\\')+1);
    $splited = explode(',', $contactForm['image']);
    $binary = base64_decode($splited[1]);
    file_put_contents($imageLink, $binary);
    // Sauvergarder le lien $imageLink en DB


}

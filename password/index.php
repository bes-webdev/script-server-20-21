<?php

$users = [
    ['login' => 'jona', 'password'=> '$2y$10$.WYiVyoV.ZxdttModdRmU.P9L2We/KJmSZdwWqDKNMEcy1eduzzyO'], //bes
    ['login' => 'florian', 'password'=> '$2y$10$e2Cj6CeqSKzueo4ahif6uuzEuVyk/DLbK2MAqW7h2am5xS9WCbgo.'], //toto
];


if (getConnectedUser($users)) {
    secureContent();
}else if (isFormSubmit()) {
    $user = getUser($_POST['login'], $users);
    if ($user !== null) {
        if (passwordValid($user, $_POST['password'])) {
            logUser($user['login']);
            redirect();
        } else {
            formError();
            formLogin();
        }
    } else {
        formError();
        formLogin();
    }
}else {
    formLogin();
}

function redirect() {
    header('Location: /password');
}


function getConnectedUser($users) {
    if (isset($_COOKIE['token'])) {
        return getUser($_COOKIE['token'], $users);
    }
    return null;
}

function getUser($login, $users) {
    foreach ($users as $user) {
        if ($login === $user['login']) {
            return $user;
        }
    }
    return null;
}

function passwordValid($user, $password) {
   return password_verify($password, $user['password']);
}

function logUser($login) {
    setcookie('token', $login, time() + 3600, "","", true, true); // Attention, mettre le login dans le cookie est très mauvais
}

function formError() {
    echo '<p>Bad credentials</p>';
}

function isFormSubmit() {
    return isset($_POST['login']) && isset($_POST['password']);
}

function formLogin() {
    echo '<form action="" method="post">
    <div>
        <label for="login">login</label>
        <input id="login" type="text" name="login">
    </div>
    <div>
        <label for="password">Mot de passe</label>
        <input id="password" type="password" name="password">
    </div>
    <div>
        <input type="submit">
    </div>
</form>';
}

function secureContent() {
    echo 'CONTENU TOP SECRET';
}

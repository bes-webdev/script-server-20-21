<?php
//<input type="text" name="test" value="'.(isset($_GET['test'])?$_GET['test']:'').'">
echo '
<form action="">
<textarea name="text" id="" cols="60" rows="10"></textarea>
<button type="submit">SEND</button></form>';

$regex = "#^04[4-9]\d(([ .\/-]?\d{2}){3}|([ .\/-]?\d{3}){2})$#";
$regexLink = "#https?://([^ ]+)#";
$nl2br = "#\r\n|\n\r|\r|\n#s";
//$regex = "#^04[4-9]\d$#";

if (isset($_GET['test'])) {
    testMatch($_GET['test'], $regex);
}

if (isset($_GET['text'])) {
    $text = replaceLink($_GET['text'], $regexLink);
    $text = replaceLines($text, $nl2br);
    echo $text;
}

function testMatch($string, $pattern) {
    if (preg_match($pattern, $string)) {
        echo 'MATCH '.$pattern. ' dans '.$string;
    } else {
        echo 'NO MATCH '.$pattern. ' dans '.$string;
    }
}

function replaceLink($string, $pattern) {
    return preg_replace($pattern, '<a href="$0">$1</a>', $string);
}

function replaceLines($string, $pattern) {
    return preg_replace($pattern, '<br>', $string);
}

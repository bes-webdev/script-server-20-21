<?php

$languages = ['fr', 'nl', 'en'];
$bonjour= ['fr' => 'Bonjour', 'nl' => 'Goede morgen', 'en' => 'Hello'];
$menu = [
    'presentation' => ['fr' => 'presentation', 'nl' => 'Presentation en nl', 'en' => 'About us'],
    'realisations' => ['fr' => 'réalisations', 'nl' => 'prestaties', 'en' => 'Portfolio'],
    'contact' => ['fr' => 'Contactez-nous', 'nl' => 'Contact', 'en' => 'Contact Us']
];

$lang = 'fr';
// isset retourne truc si l'argument est défini, sinon false
if (isset($_GET['lang']) && in_array($_GET['lang'], $languages)) {
    $lang = $_GET['lang'];
} else if (isset($_GET['lang']) && !in_array($_GET['lang'], $languages)) {
    echo 'langue invalide, petit comique <br>';
}

// On cherche la page.
$pages = array_keys($menu);
$page = $pages[0]; // Page par defaut
if (isset($_GET['page']) && in_array($_GET['page'], $pages)) {
    $page = $_GET['page'];
} else if (isset($_GET['page']) && !in_array($_GET['page'], $pages)) {
    http_response_code(404);
    echo 'la page demandée n\'existe pas';
    die();
}

echo $bonjour[$lang];
//
//echo '<ul>
//    <li><a href="?lang=fr">FR</a></li>
//    <li><a href="?lang=en">EN</a></li>
//    <li><a href="?lang=nl">NL</a></li>
//</ul>';

/* Exercice :
 1- Réécrire le menu en fonction du tableau $languages
 2- Ajouter 'style="color: red;"' au <a> de langue sélectionnée
*/

echo '<ul>';
foreach ($languages as $language) {
    echo '<li><a href="?lang='.$language.'"';
    if ($language === $lang) {
        echo ' style="color:red;"';
    }
    echo '>'.strtoupper($language).'</a></li>';
}
echo '</ul>';

echo '<nav>
    <ul>';
    // Afficher le menu dans la bonne langue
    foreach ($menu as $menuKey => $menuElement) {
        echo '<li><a href="?lang='.$lang.'&page='.$menuKey.'"';
        if ($menuKey === $page) {
            echo ' style="color:red;"';
        }
        echo '>'.$menuElement[$lang].'</a></li>';
    }

echo '</ul>
</nav>';




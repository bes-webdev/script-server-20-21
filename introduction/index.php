<?php

$var = "Bonjour et bienvenue en script server";

// Array "classique"
$students = [
    'Remi',
    'Remy',
    'Mehdi',
    'Florian'
];

echo count($students) . " élèves présents <br>" ;
echo '<h1>Structure du tableau classique</h1>';
var_dump($students);
echo '<br>';

// Tableau associatif
$studentsAssociatif = [
    'Remi' => 'Fisette' ,
    'Remy' => 'Lecomte',
    'Mehdi' => 'El-assry',
    'Florian' => 'Davila Gomez'
];

echo '<h1>Structure du tableau associatif</h1>';
var_dump($studentsAssociatif);
echo '<h2>Prendre un élément particulier</h2>';
echo $studentsAssociatif['Remi'];

echo '<h1> Boucle sur tableau normal</h1>';
echo '<ul>';
for($i = 0; $i < count($students); $i ++) {
    echo '<li>'.$students[$i].'</li>';
}
echo '</ul>';


echo '<h1> Boucle sur tableau associatif</h1>';
echo '<ul>';
// Ne fonctionne pas car pas de clé numérique sur le tableau associatif
for($i = 0; $i < count($studentsAssociatif); $i ++) {
    echo '<li>'.$studentsAssociatif[$i].'</li>';
}
echo '</ul>';

// Le foreach
echo '<h2>Boucle avec foreach</h2>';
echo '<h3>Foreach sur tableau associatif</h3>';
echo '<ul>';
foreach ($studentsAssociatif as $key => $value) { // L'écriture $key => $value correspond à la manière dont on déclare un tableau associatif (voir plus haut ligne 19).
    echo '<li>'.$key.' '.$value.'</li>';
}
echo '</ul>';

echo '<h4>sans la clé</h4>';
foreach ($studentsAssociatif as $value) { // Si on enlève l'écriture $key => $value on ne récupère que la value
    echo '<li>'.$value.'</li>';
}
echo '</ul>';

echo '<h4>Sur un tableau classique</h4>';
foreach ($students as $value) { // J'aurais pu écrire foreach ($students as $student)
    echo '<li>'.$value.'</li>';
}
echo '</ul>';
